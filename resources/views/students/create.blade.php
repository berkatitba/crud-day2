<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mahasiswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>
  <body>

  <div class="container">
        <div class="row" style="margin:25px;">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class='text-center mb-5'>Tambah Data Mahasiswa Baru</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('student') }}" method="post">

                        {!! csrf_field() !!}

                        <label>NIM</label></br>
                        <input type="text" name="nim" id="nim" class="form-control"></br>

                        <label>Nama</label></br>
                        <input type="text" name="name" id="name" class="form-control"></br>

                        <label>Jenis Kelamin</label></br>
                        <input type="text" name="gender" id="gender" class="form-control"></br>

                        <label>Prodi</label></br>
                        <input type="text" name="prodi" id="prodi" class="form-control"></br>

                        <label>Email</label></br>
                        <input type="text" name="email" id="email" class="form-control"></br>

                        <label>Alamat</label></br>
                        <input type="text" name="address" id="address" class="form-control"></br>

                        <input type="submit" value="Save" class="btn btn-success"></br>
                    </form>
  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
  </body>
</html>