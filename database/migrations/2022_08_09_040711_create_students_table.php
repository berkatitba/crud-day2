<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->integer('nim')->nullable(false);
            $table->string('name')->nullable(false);
            $table->enum('gender', ['Laki-laki', 'Perempuan'])->nullable(false);
            $table->string('prodi')->nullable(false);
            $table->string('email')->nullable(false);
            $table->string('address')->nullable(false);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
